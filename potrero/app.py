__version__ = '0.1.0'
#!/usr/bin/env python3

from peewee import *
from playhouse.shortcuts import model_to_dict, dict_to_model
from flask import Flask, request, session, render_template, url_for, redirect
from flask_dotenv import DotEnv
import flask_admin as admin
from flask_admin import BaseView, expose
from flask_admin.contrib.peewee import ModelView
from flask_babelex import Babel
from flask_restful import Resource, Api, abort

app = Flask(__name__, instance_relative_config=True)
env = DotEnv(app)
babel = Babel(app)
admin = admin.Admin(app, name='Potrero')
api = Api(app)
db = SqliteDatabase('potrero.db')

app.secret_key = '\xfd{H\xe5<\x95\xf9\xe3\x96.5\xd1\x01O<!\xd5\xa2\xa0\x9fR"\xa1\xa8'


class ModelBase(Model):
    class Meta:
        database = db

class User(ModelBase):
    username = CharField()

    def __str__(self):
        return self.username

class Game(ModelBase):
    name = CharField(verbose_name='Nombre')
    logo = CharField(null=True)
    date = DateTimeField()
    location = CharField()
    price = CharField(null=True)

    def __str__(self):
        return self.name

class GameUsers(ModelBase):
    user = ForeignKeyField(User)
    game = ForeignKeyField(Game)
    admin = BooleanField(default=False)

db.connect()
db.create_tables([
    User,
    Game,
    GameUsers
])

admin.add_view(ModelView(User, name='Usuario'))
admin.add_view(ModelView(Game, name='Partido'))
admin.add_view(ModelView(GameUsers, name='Usuarios en Partidos'))

class ApiGame(Resource):
   def get(self):
       return {'hello': 'world'}

api.add_resource(ApiGame, '/api/games')

@babel.localeselector
def get_locale():
    if request.args.get('lang'):
        session['lang'] = request.args.get('lang')
    return session.get('lang', 'es')

@app.route('/')
def index():
    # return render_template('index.html')
    return redirect('/admin')

def create_app(config=None):
    return app